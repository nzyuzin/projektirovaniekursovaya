#!/usr/bin/bash

# Compiles tex into pdf
# May require several compilations
# You can view resulting pdf file with any pdf viewer you want

KURSOVAYA_TEX=$PWD/kursovaya.tex

pdflatex $KURSOVAYA_TEX
